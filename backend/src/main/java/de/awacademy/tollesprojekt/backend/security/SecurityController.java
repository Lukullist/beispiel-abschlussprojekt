package de.awacademy.tollesprojekt.backend.security;

import de.awacademy.tollesprojekt.backend.user.User;
import de.awacademy.tollesprojekt.backend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecurityController {
    private UserService userService;

    @Autowired
    public SecurityController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/api/sessionUser")
    public UserDTO sessionUser(@AuthenticationPrincipal UserDetails userDetails) {
        User sessionUser = userService.getUserByUsername(userDetails.getUsername());
        return userService.getUserDTOfromUser(sessionUser);
    }

}
