package de.awacademy.tollesprojekt.backend.gameTable;

import de.awacademy.tollesprojekt.backend.adventurer.Adventurer;
import de.awacademy.tollesprojekt.backend.user.User;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * This is the Entity class for the gametables, Entitys represent the Database entries in Java.
 * Gametables ar the representation of the group of players and a gamemaster, in which a PNP session is played.
 */

@Entity
public class GameTable {

    @Id
    @GeneratedValue
    @Column(columnDefinition = "BINARY(16)")
    private UUID id;

    private String name;

    @ManyToOne
    private User owningGM;

    @OneToMany(mappedBy = "gameTable")
    private List<Adventurer> adventurerList;

    @ManyToMany
    @JoinTable(
            name = "User_GameTable_Subscribes",
            joinColumns = {@JoinColumn(name = "gametable_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private Set<User> subscribedUsers = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "User_GameTable_Temp_Subscribes",
            joinColumns = {@JoinColumn(name = "gametable_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private Set<User> tempSubscribedUsers = new HashSet<>();

    public GameTable() {
    }

    public GameTable(String name, User owningGM) {
        this.name = name;
        this.owningGM = owningGM;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public User getOwningGM() {
        return owningGM;
    }

    public List<Adventurer> getAdventurerList() {
        return adventurerList;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOwningGM(User owningGM) {
        this.owningGM = owningGM;
    }

    public Set<User> getSubscribedUsers() {
        return subscribedUsers;
    }

    public void setSubscribedUsers(Set<User> subscribedUsers) {
        this.subscribedUsers = subscribedUsers;
    }

    public void setTempSubscribedUsers(Set<User> tempSubscribedUsers) {
        this.tempSubscribedUsers = tempSubscribedUsers;
    }

    public Set<User> getTempSubscribedUsers() {
        return tempSubscribedUsers;
    }
}
