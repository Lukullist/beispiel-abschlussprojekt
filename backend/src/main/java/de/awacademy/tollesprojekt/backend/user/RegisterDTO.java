package de.awacademy.tollesprojekt.backend.user;

public class RegisterDTO {

    private String username;
    private String password;
    private String roleInGroup;

    public RegisterDTO(String username, String password, String roleInGroup) {
        this.username = username;
        this.password = password;
        this.roleInGroup = roleInGroup;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getRoleInGroup() {
        return roleInGroup;
    }
}
