package de.awacademy.tollesprojekt.backend.user;

import de.awacademy.tollesprojekt.backend.adventurer.AdventurerDTO;
import de.awacademy.tollesprojekt.backend.gameTable.GameTableDTO;

import java.util.UUID;

public class UserProfileDTO {

    private UUID id;
    private String username;
    private String roleInGroup;
    private AdventurerDTO[] adventurers;
    private GameTableDTO[] gametables;
    private String[] adventurerGametables;

    public UserProfileDTO() {
    }

    public UserProfileDTO(UUID id, String username, String roleInGroup,
                          AdventurerDTO[] adventurers, GameTableDTO[] gametables, String[] adventurerGametables) {
        this.id = id;
        this.username = username;
        this.roleInGroup = roleInGroup;
        this.adventurers = adventurers;
        this.gametables = gametables;
        this.adventurerGametables = adventurerGametables;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRoleInGroup() {
        return roleInGroup;
    }

    public AdventurerDTO[] getAdventurers() {
        return adventurers;
    }

    public GameTableDTO[] getGametables() {
        return gametables;
    }

    public String[] getAdventurerGametables() {
        return adventurerGametables;
    }


}
