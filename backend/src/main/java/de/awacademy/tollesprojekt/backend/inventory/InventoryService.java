package de.awacademy.tollesprojekt.backend.inventory;

import de.awacademy.tollesprojekt.backend.adventurer.Adventurer;
import de.awacademy.tollesprojekt.backend.adventurer.AdventurerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InventoryService {

    private InventoryRepository inventoryRepository;
    private AdventurerService adventurerService;

    @Autowired
    public InventoryService(InventoryRepository inventoryRepository, AdventurerService adventurerService) {
        this.inventoryRepository = inventoryRepository;
        this.adventurerService = adventurerService;
    }

    /**
     * A method to convert a list of Inventory-Items to an Array of InventoryDTO's for the purpose of sending these to
     * the frontend / attaching them to adventurerDTO's to send those to the frontend.
     *
     * @param inventory The list of all the items, that DTO's are to be created for.
     * @return The created DT's.
     */
    public InventoryItemDTO [] getDTOArrayofInventoryItemsList(List<InventoryItem> inventory) {
        List<InventoryItemDTO> inventoryDTO = new ArrayList<>();
        for (InventoryItem inventoryItem : inventory) {
            InventoryItemDTO dto = new InventoryItemDTO(inventoryItem.getId(), inventoryItem.getName(), inventoryItem.getDescription(), inventoryItem.getAmount());
            inventoryDTO.add(dto);
        }
        return inventoryDTO.toArray(new InventoryItemDTO[0]);
    }

    /**
     * A method to save an Inventory-Item to the database.
     *
     * @param item The item you want to save.
     * @return The item that got saved to the database.
     */
    public InventoryItem saveItemInInventory(InventoryItem item) {
        return inventoryRepository.save(item);
    }

    /**
     * A method to delete an Inventory-Item from the database.
     * @param item The item you want to delete.
     */
    public void deleteItem(InventoryItem item) {
        inventoryRepository.delete(item);
    }

    /**
     * A method to find an item identified by it's itemname, the name of the adventurer the item belongs to and the
     * name of the user the adventurer belongs to.
     *
     * @param itemName The name of the item.
     * @param adventurerName The name of the adventurer the item belongs to.
     * @param userName The name of the user the adventurer belongs to.
     * @return The Inventory-Item, that was found. null is returned, when no item is found by the given datapoints.
     */
    public InventoryItem getItemByItemNameAdventurerNameUserName(String itemName, String adventurerName, String userName) {
        Adventurer adventurer = adventurerService.getAdventurerByAdventurerAndUserName(adventurerName, userName);
        for (InventoryItem inventoryItem : adventurer.getInventory()) {
            if(inventoryItem.getName().equals(itemName)) {
                return inventoryItem;
            }
        }
        return null;
    }


}
