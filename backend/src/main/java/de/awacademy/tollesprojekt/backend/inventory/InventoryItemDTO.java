package de.awacademy.tollesprojekt.backend.inventory;

import java.util.UUID;

public class InventoryItemDTO {

    private UUID id;
    private String name;
    private String description;
    private int amount;

    public InventoryItemDTO(UUID id, String name, String description, int amount) {
        this.name = name;
        this.description = description;
        this.amount = amount;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getAmount() {
        return amount;
    }

    public UUID getId() {
        return id;
    }
}
