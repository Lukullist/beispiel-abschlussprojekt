package de.awacademy.tollesprojekt.backend.adventurer;

import de.awacademy.tollesprojekt.backend.inventory.InventoryItemDTO;
import de.awacademy.tollesprojekt.backend.security.UserDTO;
import de.awacademy.tollesprojekt.backend.user.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

public class AdventurerDTO {

    private UUID id;
    private String name;
    private String downloadLink;
    private UserDTO user;
    private List<InventoryItemDTO> inventory;

    public AdventurerDTO(UUID id, String name, String downloadLink, UserDTO user) {
        this.name = name;
        this.downloadLink = downloadLink;
        this.user = user;
        this.id = id;
    }

    public AdventurerDTO(UUID id, String name, String downloadLink, UserDTO user, List<InventoryItemDTO> inventory) {
        this.name = name;
        this.downloadLink = downloadLink;
        this.user = user;
        this.inventory = inventory;
        this.id = id;
    }

    public AdventurerDTO() {}

    public String getName() {
        return name;
    }

    public String getDownloadLink() {
        return downloadLink;
    }

    public UserDTO getUser() {
        return user;
    }

    public List<InventoryItemDTO> getInventory() {
        return inventory;
    }

    public UUID getId() {
        return id;
    }
}
