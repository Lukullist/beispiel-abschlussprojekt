package de.awacademy.tollesprojekt.backend.adventurer;

import de.awacademy.tollesprojekt.backend.adventurer.storage.StorageProperties;
import de.awacademy.tollesprojekt.backend.inventory.InventoryItem;
import de.awacademy.tollesprojekt.backend.inventory.InventoryItemDTO;
import de.awacademy.tollesprojekt.backend.security.UserDTO;
import de.awacademy.tollesprojekt.backend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import java.util.ArrayList;
import java.util.List;

@Service
public class AdventurerService {

    private AdventurerRepository adventurerRepository;
    private UserService userService;
    private String baseUrl;

    @Autowired
    public AdventurerService(AdventurerRepository adventurerRepository,
                             UserService userService,
                             @Value("${de.pnp_tables.base_url:http://localhost:4200}") String baseUrl) {
        this.baseUrl = baseUrl;
        this.adventurerRepository = adventurerRepository;
        this.userService = userService;
    }


    /**
     * Access the database via the CRUD-Repository to get all adventurers from the database.
     *
     * @return The List of said adventurers.
     */
    public List<Adventurer> getListOfAdventurers() {
        return adventurerRepository.findAllByOrderByNameAsc();
    }

    public List<AdventurerDTO> getDTOListOfAdventurer(List<Adventurer> adventurersList) {
        List<AdventurerDTO> dtoList = new ArrayList<>();
        for (Adventurer adventurer : adventurersList) {
            AdventurerDTO dto = new AdventurerDTO(adventurer.getId(), adventurer.getName(),
                    getDownloadLinkFromPDFReference(adventurer.getPdfReference()),
                    userService.getUserDTOfromUser(adventurer.getUser()),
                    getInventoryDTOListfromAdventurer(adventurer));
            dtoList.add(dto);
        }
        return dtoList;
    }

    /**
     * Access the database via the CRUD-Repository to get all Inventory-Items for a specified adventurer.
     *
     * @param adventurer The adventurer you want to get the items for.
     * @return The list of said items.
     */
    public List<InventoryItemDTO> getInventoryDTOListfromAdventurer(Adventurer adventurer) {
        List<InventoryItemDTO> inventoryDTO = new ArrayList<>();

        for (InventoryItem item : adventurer.getInventory()) {
            inventoryDTO.add(new InventoryItemDTO(item.getId(), item.getName(), item.getDescription(), item.getAmount()));
        }

        return inventoryDTO;
    }

    /**
     * Delete an adventurer from the database.
     *
     * @param adventurer The adventurer to delete.
     */
    public void deleteAdventurer(Adventurer adventurer) {
        adventurerRepository.delete(adventurer);
    }

    /**
     * A method to automatically create a downloadlink from the PDF-Reference for our setup with the upload-dir being in
     * the root directory of the project.
     *
     * @param pDFReference The PDF-Reference to create the link for. Get this from the adventurer you want to download
     *                     the PDF from.
     * @return The downloadlink formatted as a String.
     */
    public String getDownloadLinkFromPDFReference(String pDFReference) {
        return (pDFReference.equals("") || pDFReference == null) ? "" :
                baseUrl + "/api/getFile?link=" + pDFReference;
    }

    /**
     * Save an adventurer to the database. There is a validation here, if you already have an adventurer by
     * this name with a different ID and it throws an IllegalArgumentExeption if it is triggered.
     *
     * @param adventurer The adventurer you want to save.
     * @return The adventurer the databse now has saved.
     */
    public Adventurer saveAdventurer(Adventurer adventurer) {
        for (Adventurer adventurerCheck : adventurer.getUser().getAdventurerList()) {
            if (adventurerCheck.getName().equals(adventurer.getName())
                    && !adventurerCheck.getId().equals(adventurer.getId())) {
                throw new IllegalArgumentException("Adventurer-Name already exists for this user.");
            }
        }
        return adventurerRepository.save(adventurer);
    }

    /**
     * Return an adventurer from the database with the specified username and adventurername. If the should be more then one, it returns the first.
     *
     * @param adventurerName The name of the Adventurer you want to find.
     * @param userName       The name of the User who owns the adventurer.
     * @return The found adventurer or none can be found.
     */
    public Adventurer getAdventurerByAdventurerAndUserName(String adventurerName, String userName) {
        for (Adventurer adventurer : userService.getUserByUsername(userName).getAdventurerList()) {
            if (adventurer.getName().equals(adventurerName)) {
                return adventurer;
            }
        }
        return null;
    }
}

