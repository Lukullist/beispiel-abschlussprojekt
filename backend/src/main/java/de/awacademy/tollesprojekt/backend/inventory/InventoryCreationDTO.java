package de.awacademy.tollesprojekt.backend.inventory;

public class InventoryCreationDTO {

    private InventoryItemDTO item;
    private String advName;
    private String userName;

    public InventoryCreationDTO(InventoryItemDTO item, String advName, String userName) {
        this.item = item;
        this.advName = advName;
        this.userName = userName;
    }

    public InventoryItemDTO getItem() {
        return item;
    }

    public String getAdvName() {
        return advName;
    }

    public String getUserName() {
        return userName;
    }

}
