package de.awacademy.tollesprojekt.backend.security;

import java.util.UUID;

public class UserDTO {

    private UUID id;
    private String username;
    private boolean adminStatus;
    private String roleInGroup;

    public UserDTO(UUID id, String username, boolean adminStatus, String roleInGroup) {
        this.id = id;
        this.username = username;
        this.adminStatus = adminStatus;
        this.roleInGroup = roleInGroup;
    }

    public UserDTO() {
    }

    public UUID getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public boolean isAdminStatus() {
        return adminStatus;
    }

    public String getRoleInGroup() {
        return roleInGroup;
    }
}
