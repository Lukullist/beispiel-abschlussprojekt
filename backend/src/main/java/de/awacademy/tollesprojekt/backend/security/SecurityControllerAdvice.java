package de.awacademy.tollesprojekt.backend.security;

import de.awacademy.tollesprojekt.backend.user.User;
import de.awacademy.tollesprojekt.backend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public class SecurityControllerAdvice {

    private UserService userService;

    @Autowired
    public SecurityControllerAdvice(UserService userService) {
        this.userService = userService;
    }

    @ModelAttribute("sessionUser")
    public User sessionUser(@AuthenticationPrincipal UserDetails userDetails) {
        if (userDetails != null) {
        return userService.getUserByUsername(userDetails.getUsername());
    }
        return null;
}
}
