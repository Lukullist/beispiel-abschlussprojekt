package de.awacademy.tollesprojekt.backend.user;

import de.awacademy.tollesprojekt.backend.adventurer.Adventurer;
import de.awacademy.tollesprojekt.backend.adventurer.AdventurerDTO;
import de.awacademy.tollesprojekt.backend.gameTable.GameTable;
import de.awacademy.tollesprojekt.backend.gameTable.GameTableDTO;
import de.awacademy.tollesprojekt.backend.inventory.InventoryItem;
import de.awacademy.tollesprojekt.backend.inventory.InventoryItemDTO;
import de.awacademy.tollesprojekt.backend.security.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private String baseUrl;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder,
                       @Value("${de.pnp_tables.base_url:http://localhost:4200}") String baseUrl) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.baseUrl = baseUrl;
    }

    /**
     * A method to get a list of all users in the database.
     *
     * @return The list of those users.
     */
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public UserDTO getUserDTOfromUser(User user) {
        return new UserDTO(user.getId(), user.getUsername(), user.isAdminStatus(), user.getRoleInGroup());
    }

    /**
     * A method to get a user from the database by it's username.
     *
     * @param username Said username. It should be unique.
     * @return Said user. If it returns null, there is no user with this username.
     */
    public User getUserByUsername(String username) {
        Optional<User> optionalUser = userRepository.findByUsername(username);
        return optionalUser.orElse(null);
    }

    /**
     * A method to get a user from the database by it's id.
     *
     * @param uuid Said id.
     * @return Said user. If it returns null, there is no user with this id.
     */
    public User getUserByID(UUID uuid) {
        Optional<User> optionalUser = userRepository.findById(uuid);
        return optionalUser.orElse(null);
    }


    /**
     * This method is there, so is the database is recreated, there is a user to confirm other users with, so you can
     * recreate the database content just from the web-application.
     */
    public void init() {
        if (getUsers().size() == 0) {
            User admin = new User("admin", passwordEncoder.encode("Th1s1sTheS1tuati0nTh1sP455word1sF0r"),
                    true, "visitor", true);
            userRepository.save(admin);
        }
    }

    /**
     * This method is a check, if a user of a certain username is present in the database.
     *
     * @param username The username you want to check.
     * @return True if there is one, false if there is none.
     */
    public boolean checkIfUserNamePresent(String username) {
        return userRepository.findByUsername(username).isPresent();
    }

    /**
     * This method is used to register a new user to the database.
     *
     * @param registerDTO The DTO containing all information for the new user.
     * @return The User that got saved to the database.
     */
    public User registerUser(RegisterDTO registerDTO) {
        String encodedPassword = passwordEncoder.encode(registerDTO.getPassword());
        User user = new User(registerDTO.getUsername(), encodedPassword, false
                , registerDTO.getRoleInGroup(), false);
        return userRepository.save(user);
    }

    /**
     * This method saves users to the database. It is used to either save new user or patch existing users.
     *
     * @param user The user to save. If its ID already exists you patch it, if the ID does not exist you create a new
     *             database entry.
     * @return The user  that made it to the database.
     */
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    /**
     * This method is used to dele users from the database.
     *
     * @param user The user you want to delete.
     */
    public void deleteUser(User user) {
        userRepository.delete(user);
    }


    //ToDo: Add comments

    public String buildDownloadLink(Adventurer adventurer, User sessionUser, boolean isFellow) {
        String downloadLink = "";
        if (isFellow || sessionUser.isAdminStatus()) {
            if ((adventurer.getPdfReference() != null) && !adventurer.getPdfReference().equals("")) {
                downloadLink = baseUrl + "/api/getFile?link=" + adventurer.getPdfReference();
            }
        }
        return downloadLink;
    }

    public UserProfileDTO buildUserProfileDTO(User sessionUser, User requestedUser) {
        UUID id = requestedUser.getId();
        String username = requestedUser.getUsername();
        String roleInGroup = requestedUser.getRoleInGroup();
        AdventurerDTO[] adventurers;
        GameTableDTO[] gametables;
        String[] adventurerGametables;

        boolean isFellow = false;
        if (sessionUser.getUsername().equals(requestedUser.getUsername())) {
            isFellow = true;
        } else {
            if (sessionUser.getRoleInGroup().equals("gamemaster")) {
                for (GameTable gameTable : sessionUser.getGameTableList()) {
                    if (gameTable.getSubscribedUsers().contains(requestedUser)) {
                        isFellow = true;
                        break;
                    }
                }
            } else {
                for (GameTable subscribedGameTable : requestedUser.getSubscribedGameTables()) {
                    if (subscribedGameTable.getSubscribedUsers().contains(sessionUser)) {
                        isFellow = true;
                        break;
                    }
                }
            }
        }

        List<GameTableDTO> gameTableDTOList = new ArrayList<>();

        if (requestedUser.getRoleInGroup().equals("player")) {
            List<AdventurerDTO> adventurerDTOList = new ArrayList<>();
            List<String> adventurerGameTableStringList= new ArrayList<>();

            for (Adventurer adventurer : requestedUser.getAdventurerList()) {
                String downloadLink= buildDownloadLink(adventurer, sessionUser, isFellow);
                List<InventoryItemDTO> inventoryItemDTOList = new ArrayList<>();

                if (isFellow || sessionUser.isAdminStatus()) {
                    if (adventurer.getGameTable() != null) {
                        adventurerGameTableStringList.add(adventurer.getGameTable().getId().toString());
                        for (InventoryItem inventoryItem : adventurer.getInventory()) {
                            inventoryItemDTOList.add(new InventoryItemDTO(inventoryItem.getId(),
                                    inventoryItem.getName(), inventoryItem.getDescription(), inventoryItem.getAmount()));
                        }
                    } else {
                        adventurerGameTableStringList.add("");
                    }
                }

                adventurerDTOList.add(new AdventurerDTO(
                        adventurer.getId(), adventurer.getName(), downloadLink,
                        getUserDTOfromUser(adventurer.getUser()), inventoryItemDTOList));
            }
            if (isFellow || sessionUser.isAdminStatus()) {
                for (GameTable gameTable : requestedUser.getSubscribedGameTables()) {

                    List<AdventurerDTO> adventurerList = new ArrayList<>();
                    List<UserDTO> tempSubscriberList = new ArrayList<>();
                    List<UserDTO> subscriberList = new ArrayList<>();

                    for (Adventurer adventurer : gameTable.getAdventurerList()) {
                        List<InventoryItemDTO> inventoryItemDTOList = new ArrayList<>();

                        if (isFellow || sessionUser.isAdminStatus()) {
                            for (InventoryItem inventoryItem : adventurer.getInventory()) {
                                inventoryItemDTOList.add(new InventoryItemDTO(inventoryItem.getId(),
                                        inventoryItem.getName(), inventoryItem.getDescription(), inventoryItem.getAmount()));
                            }
                        }
                        adventurerList.add(new AdventurerDTO(adventurer.getId(), adventurer.getName(),
                                buildDownloadLink(adventurer, sessionUser, isFellow),
                                getUserDTOfromUser(adventurer.getUser()), inventoryItemDTOList));
                    }

                    if (isFellow || sessionUser.isAdminStatus()) {
                        for (User subscribedUser : gameTable.getSubscribedUsers()) {
                            subscriberList.add(getUserDTOfromUser(subscribedUser));
                        }
                    }

                    if (requestedUser.getUsername().equals(sessionUser.getUsername())) {
                        for (User tempSubscribedUser : gameTable.getTempSubscribedUsers()) {
                            tempSubscriberList.add(getUserDTOfromUser(tempSubscribedUser));
                        }
                    }

                    gameTableDTOList.add(new GameTableDTO(gameTable.getId(), gameTable.getName(),
                            getUserDTOfromUser(gameTable.getOwningGM()), adventurerList.toArray(new AdventurerDTO[0]),
                            tempSubscriberList.toArray(new UserDTO[0]), subscriberList.toArray(new UserDTO[0])));
                }
            }

            adventurers = adventurerDTOList.toArray(new AdventurerDTO[0]);
            adventurerGametables = adventurerGameTableStringList.toArray(new String[0]);
        } else {
            adventurers = new AdventurerDTO[0];
            adventurerGametables = new String[0];
        }


        if (requestedUser.getRoleInGroup().equals("gamemaster")) {

            boolean isSubscriber = false;
            if (sessionUser.getUsername().equals(requestedUser.getUsername())) {
                isSubscriber = true;
            } else {
                for (GameTable gameTable : requestedUser.getGameTableList()) {
                    if (gameTable.getSubscribedUsers().contains(sessionUser)) {
                        isSubscriber = true;
                        break;
                    }
                }
            }

            for (GameTable gameTable : requestedUser.getGameTableList()) {

                List<AdventurerDTO> adventurerList = new ArrayList<>();
                List<UserDTO> tempSubscriberList = new ArrayList<>();
                List<UserDTO> subscriberList = new ArrayList<>();

                for (Adventurer adventurer : gameTable.getAdventurerList()) {
                    List<InventoryItemDTO> inventoryItemDTOList = new ArrayList<>();

                    if (isFellow || sessionUser.isAdminStatus()) {
                        for (InventoryItem inventoryItem : adventurer.getInventory()) {
                            inventoryItemDTOList.add(new InventoryItemDTO(inventoryItem.getId(),
                                    inventoryItem.getName(), inventoryItem.getDescription(), inventoryItem.getAmount()));
                        }
                    }
                    adventurerList.add(new AdventurerDTO(adventurer.getId(), adventurer.getName(),
                            buildDownloadLink(adventurer, sessionUser, isFellow),
                            getUserDTOfromUser(adventurer.getUser()), inventoryItemDTOList));
                }

                if (isSubscriber || sessionUser.isAdminStatus()) {
                    for (User subscribedUser : gameTable.getSubscribedUsers()) {
                        subscriberList.add(getUserDTOfromUser(subscribedUser));
                    }
                }

                if (requestedUser.getUsername().equals(sessionUser.getUsername())) {
                    for (User tempSubscribedUser : gameTable.getTempSubscribedUsers()) {
                        tempSubscriberList.add(getUserDTOfromUser(tempSubscribedUser));
                    }
                }

                gameTableDTOList.add(new GameTableDTO(gameTable.getId(), gameTable.getName(),
                        getUserDTOfromUser(gameTable.getOwningGM()), adventurerList.toArray(new AdventurerDTO[0]),
                        tempSubscriberList.toArray(new UserDTO[0]), subscriberList.toArray(new UserDTO[0])));
            }
        }
        gametables = gameTableDTOList.toArray(new GameTableDTO[0]);

        return new UserProfileDTO(id, username, roleInGroup, adventurers, gametables, adventurerGametables);
    }
}
