package de.awacademy.tollesprojekt.backend;

import de.awacademy.tollesprojekt.backend.adventurer.storage.StorageProperties;
import de.awacademy.tollesprojekt.backend.adventurer.storage.StorageService;
import de.awacademy.tollesprojekt.backend.security.SecurityService;
import de.awacademy.tollesprojekt.backend.user.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class BackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendApplication.class, args);
    }

    @Bean
    CommandLineRunner init(StorageService storageService, UserService userService) {
        return (args -> {
//            storageService.deleteAll();
//            storageService.init();
            userService.init();
        });
    }

}
