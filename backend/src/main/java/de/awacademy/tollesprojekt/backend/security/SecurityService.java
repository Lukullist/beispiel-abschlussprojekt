package de.awacademy.tollesprojekt.backend.security;

import de.awacademy.tollesprojekt.backend.user.User;
import de.awacademy.tollesprojekt.backend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SecurityService implements UserDetailsService {

    private UserService userService;

    @Autowired
    public SecurityService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, SecurityException {
        // TODO: Nutzer aus Datenbank laden

        User newUser = userService.getUserByUsername(username);

        if (!newUser.isConfirmed()){
            throw new SecurityException("This User is not yet confirmed");
        }

        if (newUser == null) {
            throw new UsernameNotFoundException("Username not found");
        }

        return new org.springframework.security.core.userdetails.User(
                newUser.getUsername(), newUser.getPassword(), List.of()
        );
    }
}
