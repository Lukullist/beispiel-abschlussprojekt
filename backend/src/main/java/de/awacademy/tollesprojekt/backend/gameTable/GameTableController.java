package de.awacademy.tollesprojekt.backend.gameTable;

import de.awacademy.tollesprojekt.backend.adventurer.Adventurer;
import de.awacademy.tollesprojekt.backend.adventurer.AdventurerDTO;
import de.awacademy.tollesprojekt.backend.adventurer.AdventurerService;
import de.awacademy.tollesprojekt.backend.security.UserDTO;
import de.awacademy.tollesprojekt.backend.user.User;
import de.awacademy.tollesprojekt.backend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@RestController
public class GameTableController {
    private GameTableService gameTableService;
    private UserService userService;
    private AdventurerService adventurerService;

    @Autowired
    public GameTableController(GameTableService gameTableService,
                               UserService userService,
                               AdventurerService adventurerService) {
        this.gameTableService = gameTableService;
        this.userService = userService;
        this.adventurerService = adventurerService;
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     *
     * @param sessionUser The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @return A List of all GameTables from the database as DTOs to be send to the frontend.
     */
    @GetMapping("/api/tableList")
    public GameTableDTO[] getTableList(@ModelAttribute("sessionUser") User sessionUser) {
        List<GameTable> gameTableList = gameTableService.getAllGameTables();
        return gameTableService.getDTOArrayFromList(gameTableList, false);
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     *
     * @param sessionUser      The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @param owningGMUsername The User you want to get all tables from.
     * @return All GameTables, that are owned by the specified user in form of DTOs to be send to the frontend.
     */
    @GetMapping("/api/tableListByGM")
    public GameTableDTO[] getTableList(@ModelAttribute("sessionUser") User sessionUser,
                                       @RequestParam("owningGMUsername") String owningGMUsername) {
        if (sessionUser == null) {
            return new GameTableDTO[0];
        }
        User owningGM = userService.getUserByUsername(owningGMUsername);
        if (owningGM == null) {
            return new GameTableDTO[0];
        }
        if (!owningGM.getRoleInGroup().equals("gamemaster")) {
            return new GameTableDTO[0];
        }
        List<GameTable> gameTableList = owningGM.getGameTableList();
        return gameTableService.getDTOArrayFromList(gameTableList, sessionUser.getUsername().equals(owningGMUsername));
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     *
     * @param sessionUser The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @param username    The username of the user you want to get all GameTables he is subscribed to for.
     * @return All GameTables this User is subscribed to as DTOs to be send to the frontend.
     */
    @GetMapping("/api/tableListByUser")
    public GameTableDTO[] getTableListByUser(@ModelAttribute("sessionUser") User sessionUser,
                                             @RequestParam("user") String username) {
        if (sessionUser == null) {
            return new GameTableDTO[0];
        }
        User user = userService.getUserByUsername(username);
        if (user == null) {
            return new GameTableDTO[0];
        }
        List<GameTable> gameTableList = new LinkedList<>(user.getSubscribedGameTables());
        return gameTableService.getDTOArrayFromList(gameTableList, false);
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     *
     * @param sessionUser  The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @param searchString The name or patial name you want to get all GameTables from the Database, whoose name contain said nam or partial name.
     * @return All GameTables, whoose names contain the searchstring.
     */
    @GetMapping("/api/tableListByName")
    public GameTableDTO[] getGameTableListByName(@ModelAttribute("sessionUser") User sessionUser,
                                                 @RequestParam("gameTableName") String searchString) {
        if (sessionUser == null) {
            return new GameTableDTO[0];
        }
        if (searchString == null) {
            return new GameTableDTO[0];
        }
        if (searchString.equals("")) {
            return new GameTableDTO[0];
        }
        List<GameTable> gameTableList = gameTableService.searchGameTablesByPartOfGameTableName(searchString);
        return gameTableService.getDTOArrayFromList(gameTableList, false);
    }

    /**
     *
     * @param sessionUser
     * @param gameTableID
     * @return
     */
    @GetMapping("/api/tableProfile")
    public GameTableProfileDTO getGameTableListByID(@ModelAttribute("sessionUser") User sessionUser,
                                                 @RequestParam("gametableID") String gameTableID) {
        if (sessionUser == null) {
            return null;
        }
        if (gameTableID == null) {
            return null;
        }
        if (gameTableID.equals("")) {
            return null;
        }
        GameTable gameTable = gameTableService.getGameTableById(UUID.fromString(gameTableID));
        if (gameTable == null) {
            return null;
        }
        return gameTableService.buildGameTableProfile(sessionUser, gameTable);
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     * It posts a new GameTable to the database.
     *
     * @param sessionUser  The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @param gameTableDTO The DTO carying all the info for the new GameTable.
     */
    @PostMapping("/api/newGameTable")
    public void postNewGameTable(@ModelAttribute("sessionUser") User sessionUser,
                                 @RequestBody GameTableDTO gameTableDTO) {
        if (!(gameTableDTO.getName().matches("^[a-zA-Z0-9äöüÄÖÜß ]+$")
                && (gameTableDTO.getName().length() >= 3)
                && (gameTableDTO.getName().length() <= 20))) {
            return;
        }
        if (!sessionUser.getRoleInGroup().equals("gamemaster")) return;
        User owningGM = userService.getUserByUsername(gameTableDTO.getOwningGM().getUsername());
        if (owningGM == null) return;
        List<GameTable> referenceList = owningGM.getGameTableList();
        for (GameTable gameTable : referenceList) {
            if (gameTable.getName().equals(gameTableDTO.getName())) {
                return;
            }
        }
        GameTable gameTable = new GameTable(gameTableDTO.getName(), owningGM);
        gameTableService.saveGameTable(gameTable);
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     * This method is used to get a User on the TempSubscriber list of a GameTable, to be able to after that confirm the user.
     *
     * @param sessionUser           The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @param subscribeUserTableDTO The info, which User should be subscribed to which GameTable.
     */
    @PatchMapping("/api/subscribeUserToTableRequest")
    public void subscribeUserToTableRequest(@ModelAttribute(value = "sessionUser") User sessionUser,
                                            @RequestBody SubscribeUserTableDTO subscribeUserTableDTO) {
        subscibeUserToTableHelp(sessionUser, subscribeUserTableDTO, true);
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     * This method is used to get someone from the TempSubscriber list to the Subscriber list of a GameTable.
     * Careful: He needs to be on the Temp list first for this to work.
     *
     * @param sessionUser           The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @param subscribeUserTableDTO The info, which User should be subscribed to which GameTable.
     */
    @PatchMapping("/api/subscribeUserToTable")
    public void subscribeUserToTable(@ModelAttribute(value = "sessionUser") User sessionUser,
                                     @RequestBody SubscribeUserTableDTO subscribeUserTableDTO) {
        subscibeUserToTableHelp(sessionUser, subscribeUserTableDTO, false);
    }

    /**
     * This method is there to be used by subscribeUserToTable and subscribeUserToTableRequest, because they had a very
     * similar build.
     *
     * @param sessionUser           The sessionUser, which can be inferred by @ModelAttribute("sessionUser").
     * @param subscribeUserTableDTO The DTO coming from the frontend via the ResponseBody.
     * @param temp                  For subscribeUserToTable use false, for subscribeUserToTableRequest use true.
     */
    private void subscibeUserToTableHelp(User sessionUser, SubscribeUserTableDTO subscribeUserTableDTO, boolean temp) {
        if (sessionUser == null) {
            return;
        }
        GameTable gameTable = gameTableService.getGameTableById(subscribeUserTableDTO.getGameTableId());
        User user = userService.getUserByUsername(subscribeUserTableDTO.getUserName());
        if (gameTable == null) {
            return;
        }
        if (user == null) {
            return;
        }
        if ((gameTable.getOwningGM() != sessionUser) && !temp) {
            return;
        }
        if (!sessionUser.getRoleInGroup().equals("player") && temp) {
            return;
        }
        if (!temp) {
            if (!user.getTempSubscribedGameTables().contains(gameTable)) {
                return;
            }
        } else {
            if (user.getSubscribedGameTables().contains(gameTable)) {
                return;
            }
        }
        Set<User> oldSubscribers;
        if (temp) {
            oldSubscribers = gameTable.getTempSubscribedUsers();
        } else {
            oldSubscribers = gameTable.getSubscribedUsers();
        }
        if (oldSubscribers.contains(user)) {
            return;
        }
        oldSubscribers.add(user);
        if (temp) {
            gameTable.setTempSubscribedUsers(oldSubscribers);
        } else {
            Set<User> tempSubscribers = gameTable.getTempSubscribedUsers();
            tempSubscribers.remove(user);
            gameTable.setTempSubscribedUsers(tempSubscribers);
            gameTable.setSubscribedUsers(oldSubscribers);
        }

        gameTableService.saveGameTable(gameTable);
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     * This method is used to delete someone from the TempSubscriber list of a GameTable.
     *
     * @param sessionUser The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @param userName    The username for the User to delete from the list.
     * @param stringId    The UUID for the gametable, which the user should be deleted from. It is in UUID.toString format.
     */
    @DeleteMapping("/api/deleteTempSubscribedUserFromTable")
    public void deleteTempSubscribedUserFromTable(@ModelAttribute("sessionUser") User sessionUser,
                                                  @RequestParam("userRequestName") String userName,
                                                  @RequestParam("gametableid") String stringId) {
        if (sessionUser == null) {
            return;
        }
        if (!sessionUser.getRoleInGroup().equals("gamemaster")) {
            return;
        }
        User user = userService.getUserByUsername(userName);
        if (user == null) {
            return;
        }
        UUID gameTableId = UUID.fromString(stringId);
        GameTable gameTable = gameTableService.getGameTableById(gameTableId);
        if (gameTable == null) {
            return;
        }
        if (!sessionUser.isAdminStatus() && !(gameTable.getOwningGM().getUsername().equals(user.getUsername()))) {
            return;
        }
        Set<User> tempSubscribers = gameTable.getTempSubscribedUsers();
        tempSubscribers.remove(user);
        gameTable.setTempSubscribedUsers(tempSubscribers);
        gameTableService.saveGameTable(gameTable);
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     * This method is used to delete someone from the Subscriber list of a GameTable.
     *
     * @param sessionUser The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @param userName    The username for the User to delete from the list.
     * @param stringId    The UUID for the gametable, which the user should be deleted from. It is in UUID.toString format.
     */
    @DeleteMapping("/api/deleteSubscribedUserFromTable")
    public void deleteSubscribedUserFromTable(@ModelAttribute("sessionUser") User sessionUser,
                                              @RequestParam("userRequestName") String userName,
                                              @RequestParam("gametableid") String stringId) {
        if (sessionUser == null) {
            return;
        }
        if (!(sessionUser.getRoleInGroup().equals("player") || sessionUser.getRoleInGroup().equals("visitor"))) {
            return;
        }
        User user = userService.getUserByUsername(userName);
        if (user == null) {
            return;
        }
        UUID gameTableId = UUID.fromString(stringId);
        GameTable gameTable = gameTableService.getGameTableById(gameTableId);
        if (gameTable == null) {
            return;
        }
        Set<User> subscribers = gameTable.getSubscribedUsers();
        if (subscribers.remove(user)) {
            for (Adventurer adventurer : user.getAdventurerList()) {
                if (gameTable.getAdventurerList().contains(adventurer)) {
                    adventurer.setGameTable(null);
                    adventurerService.saveAdventurer(adventurer);
                }
            }
        }
        gameTable.setSubscribedUsers(subscribers);
        gameTableService.saveGameTable(gameTable);
    }

    /**
     * This method is a controller-method and as such is supposed to be used by the frontend as a requestpoint of the backend.
     * This method is used to delete a GameTable.
     *
     * @param sessionUser   The sessionUser, can be inferred by the ControllerAdvice with @ModelAttribute("sessionUser").
     * @param owningGMName  The name of the User owning the GameTable.
     * @param gameTableName The name of the GameTable. The combination of user and GameTableName should be unique.
     */
    @DeleteMapping("/api/deleteTable")
    public void deleteGameTable(@ModelAttribute("sessionUser") User sessionUser,
                                @RequestParam("owningGM") String owningGMName,
                                @RequestParam("gameTable") String gameTableName) {
        if (!sessionUser.isAdminStatus()) {
            return;
        }
        GameTable gameTable = null;
        for (GameTable table : userService.getUserByUsername(owningGMName).getGameTableList()) {
            if (table.getName().equals(gameTableName)) {
                gameTable = table;
                break;
            }
        }
        if (gameTable != null) {
            gameTableService.deleteGameTable(gameTable);
        }
    }
}
