import {Component, ElementRef, HostListener, OnInit, AfterViewInit, ViewChild} from '@angular/core';
import {User} from '../interfaces/user';
import {SecurityService} from '../security.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit, AfterViewInit {
  sessionUser: User|null = null;

  @ViewChild('stickyMenu', {static: false}) menuElement: ElementRef;
  sticky = false;

  elementPosition: any;

  constructor(private securityService: SecurityService) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
  }

  ngAfterViewInit() {
    this.elementPosition = this.menuElement.nativeElement.offsetTop;
  }

  offsetAnchor() {
    setTimeout(() => window.scrollTo(window.scrollX, window.scrollY - 50), 0);
  }


  @HostListener('window:scroll')
  handleScroll() {
    const windowScroll = window.pageYOffset;
    if (windowScroll >= this.elementPosition) {
      this.sticky = true;
      document.body.classList.add('has-navbar-fixed-top');
    } else {
      this.sticky = false;
      document.body.classList.remove('has-navbar-fixed-top');
    }
  }

  logout() {
    this.securityService.logout();
    this.securityService.setLogInErrorPrevention('true');
  }

}
