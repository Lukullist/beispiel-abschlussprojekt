import { ValidationErrors, AbstractControl } from '@angular/forms';

export class RoleInGroupValidator {
  static roleInGroupShouldBeValid(control: AbstractControl): ValidationErrors | null {
    if (!((control.value as string) === 'player'
      || (control.value as string) === 'gamemaster'
      || (control.value as string) === 'visitor')) {
      return { shouldBeOneOfTheOptions: true };
    }
    return null;
  }
}
