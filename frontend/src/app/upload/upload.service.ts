import {Injectable, OnInit} from '@angular/core';
import {HttpClient, HttpEvent, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(private http: HttpClient) {
  }


  pushFileToStorage(file: File, userName: string, adventurerName: string): Observable<HttpEvent<{}>> {
    const formData: FormData = new FormData();

    formData.append('file', file);
    formData.append('user', userName);
    formData.append('adventurer', adventurerName);

    const req = new HttpRequest('POST', '/api/upload', formData, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);
  }

}

