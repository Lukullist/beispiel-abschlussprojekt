import {Component, OnInit} from '@angular/core';
import {UploadService} from '../upload.service';
import {HttpEventType, HttpResponse} from '@angular/common/http';
import {User} from '../../interfaces/user';
import {SecurityService} from '../../security.service';
import {AdventurerService} from '../../adventurer/adventurer.service';

@Component({
  selector: 'app-form-upload',
  templateUrl: './form-upload.component.html',
  styleUrls: ['./form-upload.component.css']
})
export class FormUploadComponent implements OnInit {

  selectedFiles: FileList;
  currentFileUpload: File;
  progress: { percentage: number } = {percentage: 0};
  sessionUser: User | null = null;
  adventurerToUploadToName: string | null = null;


  constructor(private uploadService: UploadService, private securityService: SecurityService,
              private adventurerService: AdventurerService) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u);
    this.adventurerService.getAdventurerToUploadToName().subscribe(
      s => this.adventurerToUploadToName = s
    );
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  upload() {
    this.progress.percentage = 0;

    this.currentFileUpload = this.selectedFiles.item(0);
    this.uploadService.pushFileToStorage(this.currentFileUpload, this.sessionUser.username
      , this.adventurerToUploadToName).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        console.log('File is completely uploaded!');
        this.adventurerService.setAdventurerToUploadToName(null);
        this.adventurerService.setUploadComplete('It worked!');
      }
    }, event => {
      if (event instanceof HttpResponse) {
        console.log('Something went wrong!')
        this.progress.percentage = 0;
        this.adventurerService.setAdventurerToUploadToName(null);
        this.adventurerService.setUploadError('It failed!');
      }
    });

    this.selectedFiles = undefined;
  }

}
