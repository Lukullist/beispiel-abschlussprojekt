import {Component, NgModule, OnInit} from '@angular/core';
import {User} from './interfaces/user';
import {SecurityService} from './security.service';
import { ReactiveFormsModule } from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

@NgModule({
  imports: [
    ReactiveFormsModule,
  ]
})

export class AppComponent implements OnInit {
  sessionUser: User|null = null;

  constructor(private securityService: SecurityService, private router: Router) { }


  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => {
        this.sessionUser = u;
        if (u == null) {
          this.router.navigateByUrl('/login');
        } else {
          this.router.navigateByUrl('/dashboard');
        }
      }
    );
  }
}
