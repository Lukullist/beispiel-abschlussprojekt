import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from './interfaces/user';
import {RegisterDTO} from './interfaces/RegisterDTO';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  private sessionUser = new BehaviorSubject<User | null>(null);
  private logInError = new BehaviorSubject<string | null>(null);
  private logInErrorPrevention = new BehaviorSubject<string | null>(null);

  constructor(private httpClient: HttpClient) {
    this.httpClient.get<User>('/api/sessionUser').subscribe(
      u => {
        this.sessionUser.next(u);
        // console.log(u);
      }
    );
  }

  public getSessionUser(): Observable<User | null> {
    return this.sessionUser;
  }

  public getLogInError(): Observable<string | null> {
    return this.logInError;
  }

  public getLogInErrorPrevention(): Observable<string | null> {
    return this.logInErrorPrevention;
  }

  public setLogInErrorPrevention(s: string): void {
    this.logInErrorPrevention.next(s);
  }

  public login(username: string, password: string) {
    this.httpClient.get<User>('/api/sessionUser', {
      headers: {
        authorization: 'Basic ' + btoa(username + ':' + password)
      }
    }).subscribe(
      u => this.sessionUser.next(u),
      () => {
        this.sessionUser.next(null);
        this.logInError.next('Your login was rejected by the backend.');
      },
    );
  }

  public logout() {
    this.httpClient.post('/api/logout', null).subscribe(
      () => this.sessionUser.next(null),
    );
  }

  public register(username: string, password: string, roleInGroup: string): Observable<User | null> {
    // ToDo: Implement validation
    if (username === '' || password === '' || roleInGroup === '') {
      return;
    }
    const registerDTO: RegisterDTO = {
      username,
      password,
      roleInGroup,
    };
    return this.httpClient.post<User | null>('/api/register', registerDTO);
  }

  public deleteUser(userName: string): Observable<any> {
    return this.httpClient.delete('/api/deleteUser',
      {params: {userToDelete: userName}});
  }

  public confirmUser(user: User): Observable<any> {
    return this.httpClient.patch('/api/confirmUser', user);
  }
}
