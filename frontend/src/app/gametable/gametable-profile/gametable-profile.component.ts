import {Component, OnInit} from '@angular/core';
import {User} from '../../interfaces/user';
import {Gametable} from '../../interfaces/gametable';
import {HttpClient} from '@angular/common/http';
import {SecurityService} from '../../security.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {GametableService} from '../gametable.service';
import {GametableProfile} from '../../interfaces/gametableProfile';

@Component({
  selector: 'app-gametable-profile',
  templateUrl: './gametable-profile.component.html',
  styleUrls: ['./gametable-profile.component.css']
})
export class GametableProfileComponent implements OnInit {

  sessionUser: User | null = null;
  name: Params | null = null;
  gametable: GametableProfile | null = null;
  adventurerToShowInventoryForName: string | null = null;


  constructor(private httpClient: HttpClient, private securityService: SecurityService, private gameTableService: GametableService,
              private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      sessionUser => {
        this.sessionUser = sessionUser;
        this.route.params.subscribe(name => {
            this.name = name;
            this.getTableById();
          }
        );
      }
    );
  }

  setAdventurerToShowInventoryFor(adventurername: string) {
    if (this.adventurerToShowInventoryForName === adventurername) {
      this.adventurerToShowInventoryForName = null;
    } else {
      this.adventurerToShowInventoryForName = adventurername;
    }
  }

  deleteTempSubscribedUser(user: User) {
    this.gameTableService.deleteTempSubscribedUser(user, this.gametable)
      .subscribe(() => this.getTableById());
  }

  deleteSubscribedUser(user: User) {
    this.gameTableService.deleteSubscribedUser(user, this.gametable)
      .subscribe(() => this.getTableById());
  }

  getTableById() {
    this.httpClient.get<GametableProfile>('/api/tableProfile', {params: this.name}).subscribe(gametable => {
      this.gametable = gametable;
    });
  }
}
