import {Injectable, Optional} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../interfaces/user';
import {Gametable} from '../interfaces/gametable';

@Injectable({
  providedIn: 'root'
})
export class GametableService {

  constructor(private httpClient: HttpClient) {
  }

  public deleteTable(owningGMName: string, gameTableName): Observable<any> {
    return this.httpClient.delete('/api/deleteTable',
      {params: {owningGM: owningGMName, gameTable: gameTableName}});
  }

  public getListOfTablesByGM(username: string): Observable<any> {
    return this.httpClient.get<Gametable[]>('/api/tableListByGM',
      {params: {owningGMUsername: username}});
  }

  public getListOfTables(): Observable<any> {
    return this.httpClient.get<Gametable[]>('/api/tableList');
  }

  deleteTempSubscribedUser(user: User, gametable: Gametable): Observable<any> {
    return this.httpClient.delete('/api/deleteTempSubscribedUserFromTable',
      {params: {userRequestName: user.username, gametableid: gametable.id.toString()}});
  }

  deleteSubscribedUser(user: User, gametable: Gametable): Observable<any> {
    return this.httpClient.delete('/api/deleteSubscribedUserFromTable',
      {params: {userRequestName: user.username, gametableid: gametable.id.toString()}});
  }


}
