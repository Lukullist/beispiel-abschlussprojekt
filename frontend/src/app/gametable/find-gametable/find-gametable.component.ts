import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SecurityService} from '../../security.service';
import {Gametable} from '../../interfaces/gametable';
import {User} from '../../interfaces/user';
import {SubscribeUserTableDTO} from '../../interfaces/SubscribeUserTableDTO';
import {v4} from 'uuid/interfaces';

@Component({
  selector: 'app-find-gametable',
  templateUrl: './find-gametable.component.html',
  styleUrls: ['./find-gametable.component.css']
})
export class FindGametableComponent implements OnInit {
  sessionUser: User | null = null;
  gametableName = '';
  gametableList: Gametable[] = [];

  constructor(private httpClient: HttpClient,
              private securityService: SecurityService) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
  }

  findTablesByGameTableName() {
    this.httpClient.get<Gametable[]>('/api/tableListByName',
      {params: {gameTableName: this.gametableName}}).subscribe(list => {
      this.gametableList = list;
      console.log(list);
    });
    this.gametableName = '';
  }

  subscribeTableRequest(id: v4) {
    const subscribeUserTableDTO: SubscribeUserTableDTO = {
      userName: this.sessionUser.username,
      gameTableId: id,
    };
    this.httpClient.patch<void>('/api/subscribeUserToTableRequest', subscribeUserTableDTO).subscribe(() => {
      this.gametableList = [];
    });
  }

  checkForSessionuser(gametable: Gametable) {
    return ((gametable.subscribedUserList.filter(u => u.username === this.sessionUser.username).length > 0)
      || (gametable.tempSubscribedUserList.filter(u => u.username === this.sessionUser.username).length > 0));
  }

}
