import {Component, Input, OnInit} from '@angular/core';
import {User} from '../interfaces/user';
import {HttpClient} from '@angular/common/http';
import {SecurityService} from '../security.service';
import {Adventurer} from '../interfaces/adventurer';
import {AdventurerService} from '../adventurer/adventurer.service';
import {InventoryItem} from '../interfaces/inventoryItem';
import {UploadService} from '../upload/upload.service';
import {InventoryService} from '../inventory/inventory.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Gametable} from '../interfaces/gametable';
import {GametableService} from '../gametable/gametable.service';
import {UserProfile} from '../interfaces/userProfile';
import {v4} from 'uuid/interfaces';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {


  sessionUser: User | null = null;
  id: Params | null = null;
  userProfile: UserProfile | null = null;
  adventurerToShowInventoryForName: string | null = null;


  constructor(private httpClient: HttpClient, private securityService: SecurityService,
              private adventurerService: AdventurerService, private uploadService: UploadService,
              private gametableService: GametableService, private inventoryService: InventoryService,
              private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      sessionuser => {
        this.sessionUser = sessionuser;
        this.route.params.subscribe(id => {
          this.id = id;
          this.getUserProfile();
        });

      }
    );
    if (this.sessionUser === null) {
      this.router.navigateByUrl('/login');
      console.log('You were redirected to login, because you are not logged in');
      return;
    }
  }

  getUserProfile() {
    this.httpClient.get<UserProfile>('/api/userProfile', {params: this.id}).subscribe(userProfile => {
      this.userProfile = userProfile;
      console.log(userProfile);
    });
  }

  setAdventurerToShowInventoryFor(adventurername: string) {
    if (this.adventurerToShowInventoryForName === adventurername) {
      this.adventurerToShowInventoryForName = null;
    } else {
      this.adventurerToShowInventoryForName = adventurername;
    }
  }

}

