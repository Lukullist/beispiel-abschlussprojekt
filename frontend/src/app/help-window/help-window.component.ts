import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-help-window',
  templateUrl: './help-window.component.html',
  styleUrls: ['./help-window.component.css']
})

export class HelpWindowComponent implements OnInit {
  @Input() myMessage: string;
  constructor() { }

  ngOnInit() {
  }

}
