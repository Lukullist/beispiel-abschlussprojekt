import {Injectable} from '@angular/core';
import {Adventurer} from '../interfaces/adventurer';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../interfaces/user';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AdventurerService {

  private adventurerToUploadToName = new BehaviorSubject<string | null>(null);
  private adventurerToChooseTableForName = new BehaviorSubject<string | null>(null);
  private uploadComplete = new BehaviorSubject<string | null>(null);
  private adventurerListChanged = new BehaviorSubject<string | null>(null);

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  public getAdventurerToUploadToName(): Observable<string | null> {
    return this.adventurerToUploadToName;
  }

  public setAdventurerToUploadToName(adventurername: string): void {
    this.adventurerToUploadToName.next(adventurername);
  }

  public getAdventurerToChooseTableForName(): Observable<string | null> {
    return this.adventurerToChooseTableForName;
  }

  public setAdventurerToChooseTableForName(adventurername: string): void {
    this.adventurerToChooseTableForName.next(adventurername);
    this.router.navigateByUrl('/gametables');
  }

  public getUploadComplete(): Observable<string | null> {
    return this.uploadComplete;
  }

  public setUploadComplete(message: string): void {
    this.uploadComplete.next(message);
  }

  public setUploadError(message: string): void {
    this.uploadComplete.error(message);
  }

  public getAdventurerListChanged(): Observable<string | null> {
    return this.adventurerListChanged;
  }

  public setAdventurerListChanged(message: string): void {
    this.adventurerListChanged.next(message);
  }

  public getAdventurerList(): Observable<Adventurer[] | null> {
    return this.httpClient.get<Adventurer[]>('/api/adventurerList');
  }

  public getAdventurerListForUser(user: User): Observable<Adventurer[] | null> {
    return this.httpClient.get<Adventurer[]>('/api/adventurerListofUser',
      {params: {user: user.username}});
  }

  public deleteAdventurer(adventurer: Adventurer): Observable<any> {
    return this.httpClient.delete('/api/deleteAdventurer',
      {params: {adventurerName: adventurer.name, userName: adventurer.user.username}});
  }
}
