import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
// @ts-ignore
import {InventoryItem} from '../interfaces/inventoryItem';
import {User} from '../interfaces/user';
import {SecurityService} from '../security.service';
import {Adventurer} from '../interfaces/adventurer';



@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {

  sessionUser: User | null = null;
  adventurer: Adventurer;
  item: InventoryItem;

  constructor(private http: HttpClient, private securityService: SecurityService) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
  }
}
