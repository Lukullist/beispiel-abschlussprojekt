import { Injectable } from '@angular/core';
import {InventoryItem} from '../interfaces/inventoryItem';
import {Adventurer} from '../interfaces/adventurer';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {InventoryCreationDTO} from '../interfaces/InventoryCreationDTO';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {

  constructor(private http: HttpClient) { }

  deleteInventoryItem(item: InventoryItem, adventurer: Adventurer): Observable<any> {
    return this.http.delete('/api/deleteItem',
      {params: {itemName: item.name, adventurerName: adventurer.name, userName: adventurer.user.username}});
  }

  addItem(newItem: InventoryItem, adventurerName: string, user: string): Observable<any> {
    const inventoryCreation: InventoryCreationDTO = {item: newItem, advName: adventurerName,
      userName: user};
    return this.http.post('/api/addItem', inventoryCreation);
  }

  changeAmount(itemToChange: InventoryItem, adventurerName: string, user: string): Observable<any> {
    const inventoryCreation: InventoryCreationDTO = {item: itemToChange, advName: adventurerName, userName: user};
    console.log(inventoryCreation);
    return this.http.patch('/api/changeAmountOfItem', inventoryCreation);
  }


}
