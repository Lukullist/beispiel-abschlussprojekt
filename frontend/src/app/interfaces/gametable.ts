import {Adventurer} from './adventurer';
import {v4} from 'uuid/interfaces';
import {User} from './user';

export interface Gametable {
  id: v4;
  name: string;
  owningGM: User;
  adventurerList: Adventurer[];
  tempSubscribedUserList: User[];
  subscribedUserList: User[];
}
