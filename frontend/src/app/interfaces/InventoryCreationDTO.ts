import {InventoryItem} from './inventoryItem';

export interface InventoryCreationDTO {
  item: InventoryItem;
  advName: string;
  userName: string;
}
