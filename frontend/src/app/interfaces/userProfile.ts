import {v4} from 'uuid/interfaces';
import {Adventurer} from './adventurer';
import {Gametable} from './gametable';

export interface UserProfile {
  id: v4;
  username: string;
  roleInGroup: string;
  adventurers: Adventurer[];
  gametables: Gametable[];
  adventurerGametables: string[];
}
