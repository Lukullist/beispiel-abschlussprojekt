import {v4} from 'uuid/interfaces';

export interface SubscribeUserTableDTO {
  userName: string;
  gameTableId: v4;
}
