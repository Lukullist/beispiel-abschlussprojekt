import {v4} from 'uuid/interfaces';
import {Adventurer} from './adventurer';
import {Gametable} from './gametable';
import {User} from './user';

export interface GametableProfile {
  id: v4;
  name: string;
  owningGM: User;
  adventurerList: Adventurer[];
  tempSubscribedUserList: User[];
  subscribedUserList: User[];
}
