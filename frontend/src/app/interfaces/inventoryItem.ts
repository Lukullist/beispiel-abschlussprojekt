import {v4} from 'uuid/interfaces';

export interface InventoryItem {
  id: v4;
  name: string;
  description: string;
  amount: number;
}
