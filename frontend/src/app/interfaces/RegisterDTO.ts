export interface RegisterDTO {
  username: string;
  password: string;
  roleInGroup: string;
}
